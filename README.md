##Domain Pixel Tracker##

In the advertising industry one of the more important issues for advertisers is the ability to validate that they are running their ads on the correct domain that they are paying for. For example Verizon does not want to be running advertisements on break.com when they are paying for ads on huffingtonpost.com. In order to combat this advertisers and networks run pixel trackers to track the domain where an ad appears on every page load.

One issue faced is breaking out of an iframe in order to determine the true URL of a given page load. Sometimes, domain urls are masked by iFrames and code must be implemented to find the true URL.

###Requirements:###

* Must be pure JavaScript (no additional libraries, ie jQuery)
* Must work on IE 9 (ECMAScript 5)
* True domain must be captured
* Domain must be sent back to database and stored for retrieval later
* Discuss how you would make this a scalable solution (100 million or more page views a day)