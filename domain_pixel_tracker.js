//Collect data to be sent
var data = {};
if (window.self !== window.top) { //detect if ad is running inside an iframe; this method works because the advertiser/network controls the ad being sent over and can ensure that other referrals are not occuring before this script runs
  data['publisher_url'] = document.referrer; //if ad is in an iframe, use document referrer to establish the publisher site url
} else {
  data['publisher_url'] = window.location.href; //otherwise, use window.location.href to establish the publisher site url
}
data['publisher_domain'] = data['publisher_url'].match(/:\/\/(.[^/]+)/)[1]; //extract the domain for the publisher site

//could also send other data at this point like an ID for the ad, browser info, etc

//walk through data array and encode for url
var url_encode_data = new Array();
for(var key in data){
    url_encode_data.push(key + '=' + encodeURIComponent(data[key]));
}
query_string = url_encode_data.join('&');

(new Image()).src = (window.location.protocol == "https:" ? "https:" : "http:") + "//megseegmiller.com/url_to_save_impression_to_db?" + query_string;
/*
My first thought was to send the data via an XMLHttpRequest rather than slowing down the client or the server with an (arguably small) image load, but support is spotty (only modern browsers / IE 10+). I still wanted to save load time (especially when thinking about server load when scaling 100M+ views per day) by not actually loading an image. I decided to create a JavaScript image object which doesn't insert the image into the DOM (so no broken or hidden images). The url at which the impression is saved should return a status of 'OK' to avoid a url not found error.

I did not wrap the code in functions nor scope out variables in order to save characters / bytes which add up with 100M+ impressions. In production, variable names should be made more unique or prefixed to prevent interactiosn with scripts the publisher may be running.
*/